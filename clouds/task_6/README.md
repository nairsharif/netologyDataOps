### Домашнее задание «DevOps и автоматизация в Yandex.Cloud »

#### Цель: 
Создать serverless контейнера с помощью terraform 
#### Задание:

Создайте контейнер используя код из [примера документации](https://cloud.yandex.ru/docs/serverless-containers/quickstart#primery-prilozhenij-i-dockerfile) и положите его в container registry
Опишите деплой serverless контейнера с помощью terraform

#### Результат:

Dockerfile
Файлы для терраформ ( удалив секреты)
Ссылка на работающий контейнер

#### Инструменты:
Создание serverless контейнера - [https://cloud.yandex.ru/docs/serverless-containers/operations/create](https://cloud.yandex.ru/docs/serverless-containers/operations/create)